import React, { Component } from 'react';
import classes from './App.css';

import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';

class App extends Component {

  constructor(props) {
    super(props);
    console.log('[App.js] Inside Constructor', props)
  }

  componentWillMount() {
		console.log('[App.js] Inside component will mount')
  }

	componentDidMount() {
		console.log('[App.js] Inside component did mount')
	}

  state = {
    persons: [
        {id: 2, name: 'Alex', age: 25},
        {id: 3, name: 'John', age: 24},
        {id: 4, name: 'Smith', age: 22},
    ],
    showPersons: false
  };


  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = {
      ...this.state.persons[personIndex]
    };
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState( {persons: persons} );
  };

  deletePersonHandler = (personIndex) => {
    const persons = this.state.persons.slice();
    persons.splice(personIndex, 1);
    this.setState({persons: persons});
  };

	togglePersonsHandler = (event) => {
	  const currentState = !this.state.showPersons;
	  this.setState({showPersons : currentState});
  };

  render() {
		console.log('[App.js] Inside render');

    let persons = null;

    if (this.state.showPersons) {
      persons = (
				<div>
          <Persons
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed={this.nameChangeHandler}
          />
				</div>
      );
    }

    return (
      <div className={classes.App}>
        <Cockpit
          showPersons={this.state.showPersons}
          persons={this.state.persons}
          clicked={this.togglePersonsHandler}
        />
        {persons}
      </div>
    );
  }
}

export default App;
