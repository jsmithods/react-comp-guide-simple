import React, {Component} from 'react';

import Person from './Person/Person';

class Persons extends Component {

	constructor(props) {
		super(props);
		console.log('[Persons.js] Inside Constructor', props)
	}

	componentWillMount() {
		console.log('[Persons.js] Inside component will mount')
	}

	componentDidMount() {
		console.log('[Persons.js] Inside component did mount')
	}

	componentWillReceiveProps(nextProps) {
		console.log('[ UPDATE Persons.js] will receive props', nextProps)
	}

	shouldComponentUpdate(nextProps, nextState) {
		console.log('[ UPDATE Persons.js] should component update', nextProps, nextState);
		return nextProps.persons !== this.props.persons;
	}

	componentWillUpdate(nextState, nextProps) {
		console.log('[ UPDATE Persons.js] component will update', nextProps, nextState);
	}

	componentDidUpdate() {
		console.log('[ UPDATE Persons.js] component did update');
	}

	render () {
		console.log('[Persons.js] Inside render');
		return this.props.persons.map((person, index) => {
			return <Person
				click={() => this.props.clicked(index)}
				name={person.name}
				age={person.age}
				key={person.id}
				changed={(event) => this.props.changed(event, person.id)}
			/>
		});
	}
}

export default Persons;