import React, {Component} from 'react'
import classes from './Person.css'

import './Person.css'

class Person extends Component {
	constructor(props) {
		super(props);
		console.log('[Person.js] Inside Constructor', props)
	}

	componentWillMount() {
		console.log('[Person.js] Inside component will mount')
	}

	componentDidMount() {
		console.log('[Person.js] Inside component did mount')
	}
  render () {
		console.log('[Persons.js] Inside render');
		return (
			<div className={classes.Person}>
				<p onClick={this.props.click}>I'm {this.props.name} and {this.props.age} Person!</p>
				<input type="text" onChange={this.props.changed} value={this.props.name} />
			</div>
		)
  }
}

export default Person